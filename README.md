# aws-ami-builder

## Description

This project will create a CIS hardened CentOS AMI using Packer.

Packer will generated the AMI like this:

1. Launch an EC2 image using the official CentOS 7 image (official AMI IDs can be found here: https://wiki.centos.org/Cloud/AWS)
1. Prepare system for hardened install
    1. Download Linux kernel (vmlinuz) initial ramdisk (initrd).
    1. Write unattended install file kickstart (.ks) to disk that is responsible for secure configuration that is happening during install (e.x. partitioning).
    1. Create GRUB entry that automatically triggers a fresh CentOS install using the files prepared on next boot.
    1. Remove sensitive files from current system.
1. Restart the EC2 instance to trigger reinstall
1. Configure the new install to be CIS compliant
    1. Remove and install packages.
    1. Configure system services.
    1. Configure network.
    1. Install and configure cloud-init so the AMI can be configured in AWS when launched.
    1. Configure grub to use the freshly installed system.
    1. Configure the system according to CIS specification.
    1. Clean the system so it can safely be used to deploy new instances.

## Building golden image

1. Before building any image you will have to install Packer. Following the official documentation to do so: https://learn.hashicorp.com/tutorials/packer/get-started-install-cli
2. Clone this repository
3. Packer will use your "default" AWS credentials to build, so update them according to the environment you want to build in
4. Follow the desired OS AMI instructions below

### centos7-x86_64-ec2.json

1. Run: 
```
% packer build -var-file vars/golden-ami-eu1.var centos7-x86_64-ec2.json
```

### rockylinux8-x86_64-ec2.json

Built the Kickstart-file from scratch and did encounter a lot of issues. It seems that one simple mistake will cause the installation to fail in dracut even before the installation process starts.

I also have noticed that the same setup can succeed sometimes and fail other times. The failure seems to be related to disk, so it is like there is a configuration difference between AWS servers.
Seems to perform better on t3.medium than t3.small.

### rockylinux8-aarch-ec2.json

Started at first working on Rocky Linux running on ARM but then got reminded that we can't run ARM just yet as Prisma nor Nessus supports ARM right now.
I abandoned this configuration when I realized this and started working on x86_64, so this one probably contains a lot of errors that needs to be fixed and it would most likeley be easier to use `rockylinux7-x86_64-ec2.json` as a base now.

### Debugging

To be able to see all the output in EC2 Serial Console do the following:

1. Add `console=ttyS0` at the end of the `centosinstall` menuentry found in `000-base.sh`.
1. Add the following lines to the GRUB configuration to have the GRUB output sent to the Serial Console:
    ```
    GRUB_TERMINAL="console serial"
    GRUB_SERIAL_COMMAND="serial --speed=115200"
    ```

### Difficulties found

Used to specify the network interface to use during the kickstart install but that caused the boot to fail and launch dracut recovery mode.
Had to debug and make sure the correct drive is being used at boot (only tested using nvme disk).
Had to update how the repository to be used is being referenced as it contained a couple of assumptions that are no longer true.

## Bulding bastion image
There needs to be one image per group of users (operations, engineering and support) and per region because:
1. Each group of users needs a different FreeIPA connection so authorization can be separated
2. Images in different regions needs to connect to different FreeIPA servers (e.x. Bastion in EU should connect to eu1svcs-cnauth00x)

Remember to update `vars/secrets.var` with proper values before building an image.

**Note** Enabling `--force-join` on the IPA join command in the LDAP script causes the connection towards FreeIPA to be created so AMIs with a connection will stop working. Only use it when building a new AMI from scratch. Otherwise, use an existing bastion AMI (with a working FreeIPA) connection.
When building a Bastion AMI using an existing Bastion AMI the FreeIPA join job will fail with "CLIENT_ALREADY_CONFIGURED", which is fine and the FreeIPA connection will continue to work (assuming it worked in the existing AMI to begin with).

Build an AMI in EU1 for Operations by running:

### CentOS 7
```
% packer build -var-file vars/secrets.var -var-file vars/bastion-ops-eu1tst.var bastion-centos7-x86_64-ec2.json
```

### Rocky Linux 8 (not tested since 2021 Q4)
```
% packer build -var-file vars/secrets.var -var-file vars/bastion-eu-ops.var bastion-rockylinux8-x86_64-ec2.json
```

The `freeipa-client` in Rocky Linux 8 is much newer than the one in CentOS 7 and requires the FreeIPA server name to be set in the server certificate SAN, unfortunately this is not the case in the Messaging environment.
