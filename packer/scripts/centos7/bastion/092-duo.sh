#!/bin/bash

yum install -y \
    openssl-devel \
    pam-devel \
    selinux-policy-devel \
    bzip2

cat > /etc/yum.repos.d/duosecurity.repo << EOT
[duosecurity]
name=Duo Security Repository
baseurl=${DUO_REPOSITORY}
enabled=1
gpgcheck=1
EOT

rpm --import https://duo.com/DUO-GPG-PUBLIC-KEY.asc
yum install -y duo_unix

cat > /etc/duo/pam_duo.conf << EOT
[duo]
; Duo integration key
ikey=${DUO_IKEY}
; Duo secret key
skey=${DUO_SKEY}
; Duo API host
host=${DUO_HOST}
; If present, Duo autentication is required for members of
; the groups, unless they begin with a question mark
group=*,!${SSH_USER}
; On service or configuration errors that prevent Duo authentication,
; fail "safe" (allow access) or "secure" (deny access)
failmode=secure
; Send command for Duo Push authentication
pushinfo=yes
; Automatically send a push login request
autopush=no
; Print the contents of /etc/motd on login
; motd=no
; Maximum number of attempts on failed login
prompts=1
; Set but unmanaged at this time
fallback_local_ip=no
accept_env_factor=yes
EOT

echo "Enabling SSH ChallengeResponseAuthentication"
sed -i 's/^ChallengeResponseAuthentication.*/ChallengeResponseAuthentication yes/g' /etc/ssh/sshd_config
echo "AuthenticationMethods publickey,keyboard-interactive" >> /etc/ssh/sshd_config

cat > /etc/pam.d/system-auth << EOT
#%PAM-1.0
# This file is auto-generated.
# User changes will be destroyed the next time authconfig is run.
auth        required      pam_env.so
auth        requisite     pam_unix.so nullok try_first_pass
auth        sufficient    pam_duo.so
auth        required      pam_faildelay.so delay=2000000
auth        [default=1 ignore=ignore success=ok] pam_succeed_if.so uid >= 1000 quiet
auth        [default=1 ignore=ignore success=ok] pam_localuser.so
#auth        sufficient    pam_unix.so nullok try_first_pass
auth        requisite     pam_succeed_if.so uid >= 1000 quiet_success
auth        sufficient    pam_sss.so forward_pass
auth        required      pam_deny.so

account     required      pam_unix.so
account     sufficient    pam_localuser.so
account     sufficient    pam_succeed_if.so uid < 1000 quiet
account     [default=bad success=ok user_unknown=ignore] pam_sss.so
account     required      pam_permit.so

password    requisite     pam_pwquality.so try_first_pass retry=3 type=
password    sufficient    pam_unix.so sha512 shadow nullok try_first_pass use_authtok
password    sufficient    pam_sss.so use_authtok
password    required      pam_deny.so

session     optional      pam_keyinit.so revoke
session     required      pam_limits.so
session     optional      pam_systemd.so
session     optional      pam_oddjob_mkhomedir.so umask=0077
session     [success=1 default=ignore] pam_succeed_if.so service in crond quiet use_uid
session     required      pam_unix.so
session     optional      pam_sss.so
EOT

cat > /etc/pam.d/sshd << EOT
#%PAM-1.0
auth       required     pam_sepermit.so
auth       required     pam_env.so
auth       sufficient   pam_duo.so
auth       required     pam_deny.so
#auth       substack     password-auth
auth       include      postlogin
# Used with polkit to reauthorize users in remote sessions
-auth      optional     pam_reauthorize.so prepare
account    required     pam_nologin.so
account    include      password-auth
password   include      password-auth
# pam_selinux.so close should be the first session rule
session    required     pam_selinux.so close
session    required     pam_loginuid.so
# pam_selinux.so open should only be followed by sessions to be executed in the user context
session    required     pam_selinux.so open env_params
session    optional     pam_oddjob_mkhomedir.so umask=0077
session    required     pam_namespace.so
session    optional     pam_keyinit.so force revoke
session    include      password-auth
session    include      postlogin
# Used with polkit to reauthorize users in remote sessions
-session   optional     pam_reauthorize.so prepare
EOT

cat > /etc/pam.d/su << EOT
#%PAM-1.0
auth            sufficient      pam_rootok.so
# Uncomment the following line to implicitly trust users in the "wheel" group.
#auth           sufficient      pam_wheel.so trust use_uid
# Uncomment the following line to require a user to be in the "wheel" group.
#auth           required        pam_wheel.so use_uid
#auth           substack        system-auth
auth            required        pam_env.so
auth            required        pam_faildelay.so delay=2000000
auth            [default=1 ignore=ignore success=ok] pam_succeed_if.so uid >= 1000 quiet
auth            [default=1 ignore=ignore success=ok] pam_localuser.so
auth            sufficient      pam_unix.so nullok try_first_pass
auth            requisite       pam_succeed_if.so uid >= 1000 quiet_success
auth            sufficient      pam_sss.so forward_pass
auth            required        pam_deny.so
auth            include         postlogin
account         sufficient      pam_succeed_if.so uid = 0 use_uid quiet
account         include         system-auth
password        include         system-auth
session         include         system-auth
session         include         postlogin
session         optional        pam_xauth.so
EOT

cat > /etc/pam.d/sudo << EOT
#%PAM-1.0
auth        required      pam_env.so
auth        required      pam_faildelay.so delay=2000000
auth        [default=1 ignore=ignore success=ok] pam_succeed_if.so uid >= 1000 quiet
auth        [default=1 ignore=ignore success=ok] pam_localuser.so
auth        sufficient    pam_unix.so nullok try_first_pass
auth        requisite     pam_succeed_if.so uid >= 1000 quiet_success
auth        sufficient    pam_sss.so forward_pass
auth        required      pam_deny.so

#auth       include      system-auth
account    include      system-auth
password   include      system-auth
session    optional     pam_keyinit.so revoke
session    include      system-auth
EOT
