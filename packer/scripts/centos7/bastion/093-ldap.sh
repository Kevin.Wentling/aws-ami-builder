#!/bin/bash

hostnamectl set-hostname bastion-${GROUP}-${ENVIRONMENT}.int.clxnetworks.net

file='/etc/sysconfig/network-scripts/ifcfg-eth0'
grep 'PEERDNS' $file 2>/dev/null

if [ $? -eq 0 ]; then
  sed -i 's/PEERDNS="yes"/PEERDNS="no"/g' $file
else
  echo PEERDNS="no" >> $file
fi

cat > /etc/resolv.conf << EOT
search int.clxnetworks.net
nameserver ${DNS_1}
nameserver ${DNS_2}
EOT

yum install -y ipa-client

/sbin/ipa-client-install \
      --domain int.clxnetworks.net \
      --mkhomedir \
      --password ${FREEIPA_PASSWORD} \
      --principal admin@CLXNETWORKS.NET \
      --realm CLXNETWORKS.NET \
      --server ${FREEIPA_SERVER_1} \
      --server ${FREEIPA_SERVER_2} \
      --hostname=bastion-${GROUP}-${ENVIRONMENT}.int.clxnetworks.net \
      --unattended \
      --verbose #\
      #--force-join

# force-join replaces existing FreeIPA connections (breaks existing bastions)

grep NISDOMAIN /etc/sysconfig/network || echo "NISDOMAIN=clxnetworks.net" >> /etc/sysconfig/network

/bin/nisdomainname | grep clxnetworks || /bin/nisdomainname clxnetworks.net

systemctl enable sssd