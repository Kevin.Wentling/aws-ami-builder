#!/bin/sh -x
#

# Strip down CentOS
dnf -y remove \
	*-firmware \
	authconfig \
	avahi \
	biosdevname \
	btrfs-progs \
	ethtool \
	pciutils-libs \
	kernel-tools \
	hwdata \
	iprutils \
	irqbalance \
	kbd \
	kexec-tools \
	linux-firmware \
	man-db \
	man-pages \
	mariadb* \
	microcode_ctl \
	plymouth \
	postfix \
	sg3_utils* \
	yum-utils \
	--setopt="clean_requirements_on_remove=1"

# Install basic set of packages
dnf -y install \
	aide \
	chrony \
	cloud-init \
	cloud-utils-growpart \
	cronie-anacron \
	dracut-config-generic \
	epel-release \
	grub2 \
	iptables-services \
	kernel #\
	#tcp_wrappers

## Install JQ, pip and boto3
#yum -y install python-pip jq
## Install latest python from pypi using pip
#pip install --upgrade \
#	pip \
#	boto3 \
#	awscli
#
#	chmod 755 /bin/aws*
