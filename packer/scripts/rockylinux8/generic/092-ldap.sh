#!/bin/bash

hostnamectl set-hostname bastion-${GROUP}.int.clxnetworks.net
dnf install -y ipa-client

/sbin/ipa-client-install \
      --domain int.clxnetworks.net \
      --mkhomedir \
      --password ${FREEIPA_PASSWORD} \
      --principal admin@CLXNETWORKS.NET \
      --realm CLXNETWORKS.NET \
      --server ${FREEIPA_SERVER_1} \
      --server ${FREEIPA_SERVER_2} \
      --unattended \
      --verbose

grep NISDOMAIN /etc/sysconfig/network || echo "NISDOMAIN=clxnetworks.net" >> /etc/sysconfig/network

/bin/nisdomainname | grep clxnetworks || /bin/nisdomainname clxnetworks.net
