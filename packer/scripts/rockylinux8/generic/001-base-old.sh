# Install wget
yum -y install wget

version=${FULL_VERSION}

# Detect primary root drive
if [ -e /dev/xvda ]; then
  drive=xvda1
elif [ -e /dev/vda ]; then
  drive=vda1
elif [ -e /dev/sda ]; then
  drive=sda1
else
  drive=$(df -hT | grep /$ | awk '{ print $1 }')
fi

#echo "lsblk"
#echo "---"
#lsblk
#echo
#echo "df -h"
#echo "---"
#df -h
#echo
#
#echo "Using root drive $drive"
#echo

#mkdir /boot/centos
#cd /boot/centos
#wget ${MIRROR_URL}/${FULL_VERSION}/os/x86_64/isolinux/vmlinuz
#wget ${MIRROR_URL}/${FULL_VERSION}/os/x86_64/isolinux/initrd.img
mkdir /boot/rockylinux
cd /boot/rockylinux
wget https://dl.rockylinux.org/stg/rocky/8.5/BaseOS/x86_64/kickstart/images/pxeboot/vmlinuz
wget https://dl.rockylinux.org/stg/rocky/8.5/BaseOS/x86_64/kickstart/images/pxeboot/initrd.img

# This kickstart file has been created to install the Core Centos7 OS and partition as per CIS CentOS Linux 7 Benchmark (v2.2.0 - 12-27-2017).
cat > /boot/rockylinux/kickstart.ks << EOKSCONFIG
# Installation settings
text
install
firstboot --disable
eula --agreed
unsupported_hardware
skipx
lang en_GB.UTF-8
keyboard uk
auth --enableshadow --passalgo=sha512
timezone UTC --isUtc

# Firewall configuration
firewall --enabled --service=ssh
# Repo settings
repo --name="AppStream" --baseurl=https://dl.rockylinux.org/stg/rocky/8.5/AppStream/x86_64/os
repo --name="base" --baseurl=https://dl.rockylinux.org/stg/rocky/8.5/BaseOS/x86_64/os/
url --url=https://dl.rockylinux.org/stg/rocky/8.5/BaseOS/x86_64/os/

# System settings
rootpw --iscrypted nothing
network --onboot yes --device eth0 --bootproto dhcp --ipv6=auto --activate
firewall --enabled --ssh
selinux --enforcing
services --enabled=sshd

# bootloader configuration
#bootloader --location=mbr --append="crashkernel=auto rhgb quiet" --timeout=0
bootloader --append="console=ttyS0,115200n8 no_timer_check crashkernel=auto net.ifnames=0 nvme_core.io_timeout=4294967295 nvme_core.max_retries=10" --location=mbr --timeout=1
reqpart
# Initialize (format) all disks
zerombr
clearpart --linux --initlabel

# Create primary system partitions
part /boot --fstype=xfs --size=512
part pv.00 --grow --size=1

# Create a volume group
volgroup vg00 --pesize=4096 pv.00

# Create LVM partitions as per CIS guide
logvol /  --fstype="xfs"  --size=2048 --name=root --vgname=vg00
logvol /tmp --fstype="xfs" --size=2048 --name=tmp --vgname=vg00 --fsoptions=nodev,noexec,nosuid
logvol /var --fstype="xfs" --size=1024 --name=var --vgname=vg00
logvol /var/tmp --fstype="xfs" --size=1024 --name=vartmp --vgname=vg00 --fsoptions=nodev,noexec,nosuid
logvol /var/log --fstype="xfs" --size=3072 --name=log --vgname=vg00
logvol /var/log/audit --fstype="xfs" --size=3072 --name=audit --vgname=vg00
logvol /home --fstype="xfs" --size=2048 --name=home --vgname=vg00 --fsoptions=nodev
# Application LV
logvol /opt --fstype="xfs" --size=4096 --name=opt --vgname=vg00

# Base Service configuration
services --enabled=sshd

# Packages selection
%packages --excludedocs
# Core only
@core
# Cloud-init is required at boot-time
cloud-init
%end

# Basic cleanup
%post

# Cleanup SSH keys
rm -f /etc/ssh/*key*
rm -rf ~/.ssh/

dnf -C -y remove linux-firmware

# Remove firewalld; it is required to be present for install/image building.
# but we dont ship it in cloud
dnf -C -y remove firewalld --setopt="clean_requirements_on_remove=1"
dnf -C -y remove avahi\*

# Let SELinux relabel FS on next boot
touch /.autorelabel
%end
reboot --eject
EOKSCONFIG

echo "menuentry 'rockylinuxinstall' {
        set root='hd0,msdos1'
    linux /boot/rockylinux/vmlinuz ip=dhcp inst.ks=hd:${drive}:/boot/rockylinux/kickstart.ks inst.repo=https://dl.rockylinux.org/stg/rocky/8.5/BaseOS/x86_64/os/ lang=en_GB.UTF-8 keymap=uk console=ttyS0
        initrd /boot/rockylinux/initrd.img
}" >> /etc/grub.d/40_custom

echo 'GRUB_DEFAULT=saved
GRUB_HIDDEN_TIMEOUT=
GRUB_TIMEOUT=2
GRUB_RECORDFAIL_TIMEOUT=5
GRUB_TERMINAL="console serial"
GRUB_SERIAL_COMMAND="serial --speed=115200"
GRUB_CMDLINE_LINUX_DEFAULT="console=tty0 quiet nosplash vga=771 nomodeset console=ttyS0"
GRUB_DISABLE_LINUX_UUID=true' > /etc/default/grub

grub2-set-default 'rockylinuxinstall'
grub2-mkconfig -o /boot/grub2/grub.cfg
grub2-mkconfig -o /boot/efi/EFI/rocky/grub.cfg

rm -rf ~/.ssh/*
rm -rf /root/*

#echo 
#echo "kickstart.ks:"
#echo "---"
#cat /boot/centos/kickstart.ks
#echo
#ls /boot/centos
#echo

#echo "--baseurl=${MIRROR_URL}${FULL_VERSION}/updates/x86_64/"

reboot
