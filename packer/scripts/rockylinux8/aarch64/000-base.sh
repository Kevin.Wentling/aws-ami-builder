# Install wget
yum -y install wget

version=${FULL_VERSION}

# Detect primary root drive
if [ -e /dev/xvda ]; then
  part=xvda1
elif [ -e /dev/vda ]; then
  part=vda1
elif [ -e /dev/sda ]; then
  part=sda1
else
  part=$(df -hT | grep /$ | awk '{ print $1 }')
fi

drive=$(lsblk -o KNAME,TYPE | grep disk | head -1 | awk '{ print $1 }')

#echo "lsblk"
#echo "---"
#lsblk
#echo
#echo "df -h"
#echo "---"
#df -h
#echo
#
#echo "Using root drive $drive"
#echo
#echo "Using partition $part"
#echo

#mkdir /boot/centos
#cd /boot/centos
#wget ${MIRROR_URL}/${FULL_VERSION}/os/x86_64/isolinux/vmlinuz
#wget ${MIRROR_URL}/${FULL_VERSION}/os/x86_64/isolinux/initrd.img
mkdir /boot/rockylinux
cd /boot/rockylinux
wget ${MIRROR_URL}${FULL_VERSION}/Minimal/${MIRROR_ARCH}/os/images/pxeboot/vmlinuz
wget ${MIRROR_URL}${FULL_VERSION}/Minimal/${MIRROR_ARCH}/os/images/pxeboot/initrd.img

# This kickstart file has been created to install the Core Centos7 OS and partition as per CIS CentOS Linux 7 Benchmark (v2.2.0 - 12-27-2017).
cat > /boot/rockylinux/kickstart.ks << EOKSCONFIG
# Use graphical install
text
firstboot --disable
eula --agreed
lang en_GB.UTF-8
keyboard uk
timezone UTC --isUtc
url --url="${MIRROR_URL}${FULL_VERSION}/BaseOS/${MIRROR_ARCH}/os/"

repo --name="AppStream" --baseurl=${MIRROR_URL}${FULL_VERSION}/AppStream/${MIRROR_ARCH}/os/

rootpw --iscrypted nothing

# Network information
network --onboot yes --device eth0 --bootproto dhcp --ipv6=auto --activate
firewall --enabled --ssh
selinux --enforcing
services --enabled=sshd

ignoredisk --only-use=${drive}
# System bootloader configuration
bootloader --append="crashkernel=auto rhgb quiet" --location=mbr --timeout=0

# Initialize (format) all disks
zerombr
clearpart --linux --drives=${drive} --initlabel

# Create primary system partitions
part /boot --fstype=xfs --size=512
part /boot/efi --fstype="efi" --size=512 --fsoptions="umask=0077,shortname=winnt"
part swap --size=${PART_SWAP}
part pv.00 --grow --size=1

# Create a volume group
volgroup vg00 --pesize=4096 pv.00

# Create LVM partitions as per CIS guide
logvol /  --fstype="xfs"  --size=${PART_ROOT} --name=root --vgname=vg00
logvol /tmp --fstype="xfs" --size=${PART_TMP} --name=tmp --vgname=vg00 --fsoptions=nodev,noexec,nosuid
logvol /var --fstype="xfs" --size=${PART_VAR} --name=var --vgname=vg00
logvol /var/tmp --fstype="xfs" --size=${PART_VARTMP} --name=vartmp --vgname=vg00 --fsoptions=nodev,noexec,nosuid
logvol /var/log --fstype="xfs" --size=${PART_VARLOG} --name=log --vgname=vg00
logvol /var/log/audit --fstype="xfs" --size=${PART_VARLOGAUDIT} --name=audit --vgname=vg00
logvol /home --fstype="xfs" --size=${PART_HOME_MIN} --grow --name=home --vgname=vg00 --fsoptions=nodev
# Application LV
logvol /opt --fstype="xfs" --size=${PART_OPT} --name=opt --vgname=vg00

%addon com_redhat_kdump --enable --reserve-mb='auto'

%end

%packages --excludedocs
@^minimal-environment
cloud-init
%end

%post --interpreter=/usr/bin/bash
# Make sure we keep the same user as before as the cloud-init user
sed -i 's/cloud-user/rocky/g' /etc/cloud/cloud.cfg

# Cleanup SSH keys
rm -f /etc/ssh/*key*
rm -rf ~/.ssh/

# Let SELinux relabel FS on next boot
touch /.autorelabel
%end

reboot --eject
EOKSCONFIG

echo "menuentry 'rockylinuxinstall' {
        set root='hd0,gpt2'
    linux /boot/rockylinux/vmlinuz ip=dhcp inst.ks=hd:${part}:/boot/rockylinux/kickstart.ks inst.repo=${MIRROR_URL}${FULL_VERSION}/Minimal/${MIRROR_ARCH}/os/ inst.lang=en_GB.UTF-8 inst.keymap=uk console=ttyS0
        initrd /boot/rockylinux/initrd.img
}" >> /etc/grub.d/40_custom

echo 'GRUB_DEFAULT=saved
GRUB_HIDDEN_TIMEOUT=
GRUB_TIMEOUT=2
GRUB_RECORDFAIL_TIMEOUT=5
GRUB_TERMINAL="console serial"
GRUB_SERIAL_COMMAND="serial --speed=115200"
GRUB_CMDLINE_LINUX_DEFAULT="console=tty0 quiet nosplash vga=771 nomodeset console=ttyS0"
GRUB_DISABLE_LINUX_UUID=true' > /etc/default/grub

grub2-set-default 'rockylinuxinstall'
grub2-mkconfig -o /boot/grub2/grub.cfg
grub2-mkconfig -o /boot/efi/EFI/rocky/grub.cfg

rm -rf ~/.ssh/*
rm -rf /root/*

reboot
